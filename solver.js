// Based on:
// Jos Stam, "Real-Time Fluid Dynamics for Games". Proceedings of the Game 
// Developer Conference, March 2003. 
// http://www.dgp.toronto.edu/people/stam/reality/Research/pub.html
// http://www.dgp.toronto.edu/people/stam/reality/Research/pdf/GDC03.pdf
// http://www.dgp.toronto.edu/people/stam/reality/Research/zip/CDROM_GDC03.zip


function IX(i,j) { return i + (N+2) * j; }

// Contribute density at the specified rate
function add_source(N, x, dxdt, dt) {
  var size = (N+2)*(N+2);
  for (var i = 0; i < size; i++) 
    x[i] += dt * dxdt[i];
}


// Set boundary conditions, assuming we're in a box. The opaque "b" parameter 
// may be 1 to indicate a horizontal component, 2 to indicate a vertical 
// component, and 0 otherwise.
function set_boundaries(N, b, x) {
  for (var i = 1; i <= N; i++) {
    x[IX(0  ,i  )] = b==1 ? -x[IX(1,i)] : x[IX(1,i)];
    x[IX(N+1,i  )] = b==1 ? -x[IX(N,i)] : x[IX(N,i)];
    x[IX(i,  0  )] = b==2 ? -x[IX(i,1)] : x[IX(i,1)];
    x[IX(i,  N+1)] = b==2 ? -x[IX(i,N)] : x[IX(i,N)];
  }
  x[IX(0  ,0  )] = 0.5 * (x[IX(1,0  )] + x[IX(0  ,1)]);
  x[IX(0  ,N+1)] = 0.5 * (x[IX(1,N+1)] + x[IX(0  ,N)]);
  x[IX(N+1,0  )] = 0.5 * (x[IX(N,0  )] + x[IX(N+1,1)]);
  x[IX(N+1,N+1)] = 0.5 * (x[IX(N,N+1)] + x[IX(N+1,N)]);
}


// Approximate a solution to system of linear equations 
//   X' = (X + a * neighbors(X)) / c
// using Gauss-Seidel relaxation
function linear_solve(N, b, x, x0, a, c) {
  for (var k = 0; k < 20; k++) {
    for (var i = 1; i <= N; i++) for (var j = 1; j <= N; j++) {
        x[IX(i,j)] = (x0[IX(i,j)] + a * (x[IX(i-1,j)] + x[IX(i+1,j)] + 
                                         x[IX(i,j-1)] + x[IX(i,j+1)])) / c;
      }
    set_boundaries(N, b, x);
  }
}


// Diffuse via Gauss-Seidel relaxation
function diffuse(N, b, x, x0, diff, dt) {
  var a = dt * diff * N * N;
  if (a == 0.0)
    for (var i = 0; i < (N+2) * (N+2); ++i) x[i] = x0[i];
  else
    linear_solve(N, b, x, x0, a, 1 + 4 * a);
}


// Advect via linear backtrace: Trace velocity backward from each cell
// center, and compute density as a linear combination of surrounding
// cells' density.
function advect(N, b, d, d0, u, v, dt) {
  var dt0 = dt * N;
  for (var i = 1; i <= N; i++) for (var j = 1; j <= N; j++) {
      var x = i - dt0 * u[IX(i,j)]; 
      if (x < 0.5)     x = 0.5; 
      if (x > N + 0.5) x = N+0.5; 
      var i0 = Math.floor(x); 
      var i1 = i0 + 1;
      var s1 = x - i0; 
      var s0 = 1 - s1; 
      
      var y = j - dt0 * v[IX(i,j)];
      if (y < 0.5)   y = 0.5; 
      if (y > N+0.5) y = N+0.5; 
      var j0 = Math.floor(y); 
      var j1 = j0+1;
      var t1 = y - j0; 
      var t0 = 1 - t1;
      
      d[IX(i,j)] = s0 * (t0 * d0[IX(i0,j0)] + t1 * d0[IX(i0,j1)]) +
        s1 * (t0 * d0[IX(i1,j0)] + t1 * d0[IX(i1,j1)]);
    }
  set_boundaries(N, b, d);
}

// Subtract the gradient of the height field to make the preceding
// operation into a mass-conserving one
function project(N, u, v, p, div) {
  for (var i = 1; i <= N; i++) for (var j = 1; j <= N; j++) {
      div[IX(i,j)] = -0.5 * (u[IX(i+1,j  )] - u[IX(i-1,j  )] + 
                             v[IX(i,  j+1)] - v[IX(i,  j-1)]) / N;
      p[IX(i,j)] = 0;
    }
  set_boundaries(N, 0, div); 
  set_boundaries(N, 0, p);
  
  linear_solve(N, 0, p, div, 1, 4);
  
  for (var i = 1; i <= N; i++) for (var j = 1; j <= N; j++) {
      u[IX(i,j)] -= 0.5 * N * (p[IX(i+1,j  )] - p[IX(i-1,j  )]);
      v[IX(i,j)] -= 0.5 * N * (p[IX(i,  j+1)] - p[IX(i,  j-1)]);
    }
  set_boundaries(N, 1, u); 
  set_boundaries(N, 2, v);
}

Number.prototype.div = function (divisor) {
  var dividend = this / divisor;
  return (dividend < 0 ? Math.ceil : Math.floor)(dividend);
};


function density_step(N, x, x0, u, v, diff, dt) {
  add_source(N,     x, x0,       dt);
  x[IX(0,     N.div(3))] = 100.0 * dt;
  x[IX(2*N.div(3), N+1)] = 0;
  diffuse(   N, 0, x0,  x, diff, dt);
  advect(    N, 0, x,  x0, u, v, dt);
}

function velocity_step(N, u, v, u0, v0, visc, dt) {
  add_source(N,    u,  u0,         dt); 
  add_source(N,    v,  v0,         dt);
  u[IX(0,     N.div(3))] = 10.0;
  v[IX(2*N.div(3), N+1)] = 100.0;
  diffuse(   N, 1, u0, u,  visc,   dt);
  diffuse(   N, 2, v0, v,  visc,   dt);
  project(   N,    u0, v0, u,  v);
  advect(    N, 1, u,  u0, u0, v0, dt); 
  advect(    N, 2, v,  v0, u0, v0, dt);
  project(   N,    u,  v,  u0, v0);
}
