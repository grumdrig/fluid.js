// global variables

var N = 64;             // Dimension of the computational grid
var diffusion = 0.000; 
var viscosity = 0.000; 
var force = 300.0;    // Force of dragging through field
var source = 600.0;  // Density of source (ddensity/dt)

var dvel = 1;           // Draw velocity vectors?

var u, v, u_prev, v_prev;  // <u,v> components of the velocity field, and previous values
var dens, dens_prev;         // density field, and previous values

var mouse_down = [0,0,0];
var omx, omy, mx, my;


/*
  ----------------------------------------------------------------------
   free/clear/allocate simulation data
  ----------------------------------------------------------------------
*/

function clear_data() {
  var size = (N+2)*(N+2);
  for (var i = 0; i < size; i++) {
    u[i] = v[i] = u_prev[i] = v_prev[i] = dens[i] = dens_prev[i] = 0.0;
  }
}

function allocate_data() {
  var size = (N+2)*(N+2);

  u = new Array(size);
  v = new Array(size);
  u_prev = new Array(size);
  v_prev = new Array(size);
  dens = new Array(size);	
  dens_prev = new Array(size);
  if (!u || !v || !u_prev || !v_prev || !dens || !dens_prev) {
    fprintf ( stderr, "cannot allocate data\n" );
    exit(1);
  }
}


/*
  ----------------------------------------------------------------------
   OpenGL specific drawing routines
  ----------------------------------------------------------------------
*/

/*
function draw_velocity() {
  var h = 1.0 / N;

  glColor4f(1.0, 0.5, 0.5, 1.0);
  glLineWidth(1.0);
  
  typedef struct Point2d { GLfloat x, y; } Point2d;
  const int VERTICES = N * N * 2;
  Point2d vertices[VERTICES];
  Point2d *vertex = vertices;
  
  for (int i = 1; i <= N; i++) {
    var x = (i - 0.5) * h;
    for (int j = 1; j <= N; j++) {
      var y = (j - 0.5) * h;
      vertex->x = x;
      vertex->y = y;
      ++vertex;
      vertex->x = x + u[IX(i,j)] / 6.0;
      vertex->y = y + v[IX(i,j)] / 6.0;
      ++vertex;
    }
  }
  
  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(2, GL_FLOAT, 0, vertices);
  
  glDisableClientState(GL_COLOR_ARRAY);
  
  glDrawArrays(GL_LINES, 0, VERTICES);
}
*/

function initialize() {
  canvas = document.getElementById("smoke");
  W = canvas.width;
  H = canvas.height;

  //canvas.getContext("2d").scale(W/N, W/N);

  allocate_data();
  clear_data();


  $("canvas").mousemove(function (e) {
      var x = e.pageX - this.offsetLeft;
      var y = e.pageY - this.offsetTop;
      motion_func(x,y);
    });

  $("canvas").mousedown(function (e) {
      var x = e.pageX - this.offsetLeft;
      var y = e.pageY - this.offsetTop;
      mouse_func(0, 1, x, y);
    });

  $("canvas").mouseup(function (e) {
      var x = e.pageX - this.offsetLeft;
      var y = e.pageY - this.offsetTop;
      mouse_func(0, 0, x, y);
    });

  //for (var i = 1; i < N; ++i)
  //  dens[IX(i,i)] = dens_prev[IX(i,i)] = source; 
}

var canvas;
var W, H;

function densityToColor(d) {
  var c = Math.floor(d * 255);
  if (c < 0) c = 0; 
  if (c > 255) c = 255;
  return "rgba(" + c + "," + c + "," + c + ",1)";
}

function densityToBrightness(d) {
  var c = Math.floor(d * 255);
  if (c < 0) c = 0; 
  if (c > 255) c = 255;
  return c;
}

function pythag(x, y) { return Math.sqrt(x * x + y * y); }

function draw_density() {
  var ctx = canvas.getContext("2d");
  var datadad = ctx.createImageData(W, H);
  var data = datadad.data;
  var D = 0;
  
  var w = W / N, h = H / N;
  for (var j = 0; j < N; j++) {
    var ds = new Array(N+1);
    var dds = new Array(N+1);
    var rs = new Array(N+1);
    var drs = new Array(N+1);
    for (var i = 0; i <= N; i++) {
      ds[i] = dens[IX(i,j)];
      dds[i] = (dens[IX(i,j+1)] - ds[i]) / h;
      rs[i] = pythag(u[IX(i,j)], v[IX(i,j)])
      drs[i] = (pythag(u[IX(i,j+1)], v[IX(i,j+1)]) - rs[i]) / h;
    }
    for (var k = 0; k < h; ++k) {
      for (var i = 0; i < N; i++) {
        var d = ds[i];
        var dd = (ds[i+1] - d) / w;
        var r = rs[i];
        var dr = (rs[i+1] - r) / w;
        for (var l = 0; l < w; ++l) {
          data[D] = densityToBrightness(d - r);
          data[D+1] = 0;
          data[D+2] = densityToBrightness(d);
          data[D+3] = 255;
          D += 4;
          d += dd;
          r += dr;
        }
        ds[i] += dds[i];
        rs[i] += drs[i];
      }
    }
  }

  ctx.putImageData(datadad, 0, 0);

  // 5  msec for N=16 with solid
  // 12 for gradient
  // 5  for N canvas-wide gradients
  // 60 for one gradient per scanline :(
  // 8  for ImageData style :)
}


/*
  ----------------------------------------------------------------------
   relates mouse movements to forces sources
  ----------------------------------------------------------------------
*/

function process_input(d, u, v, dt) {
  var size = (N+2)*(N+2);

  for (var i = 0; i < size; i++) {
    u[i] = v[i] = d[i] = 0.0;
  }

  if (mouse_down[0] || mouse_down[2]) {
    var i = mx.div(W.div(N));
    var j = my.div(H.div(N));
    
    if (i < 1 || i > N || j < 1 || j > N) 
      return;
    
    if (mouse_down[0]) {
      u[IX(i,j)] = force * (mx-omx) / W / dt;
      v[IX(i,j)] = force * (my-omy) / H / dt;
    }
    
    //if ( mouse_down[2] )
    {
      d[IX(i,j)] = source;
    }
    
    omx = mx;
    omy = my;
  }
}


/*
  ----------------------------------------------------------------------
   GLUT callback routines
  ----------------------------------------------------------------------
*/

function key_func(key) {
  switch ( key ) {
  case 'c':
  case 'C':
    clear_data();
    break;
    
  case 'v':
  case 'V':
    dvel = !dvel;
    break;
  }
}

function mouse_func(button, state, x, y) {
  omx = mx = x;
  omy = my = y;
  mouse_down[button] = state;
}

function motion_func(x, y) {
  mx = x;
  my = y;
}

function step_simulation(dt) {
  process_input(dens_prev, u_prev, v_prev, dt);
  velocity_step(N, u, v, u_prev, v_prev, viscosity, dt);
  density_step( N, dens, dens_prev, u, v, diffusion, dt);
}

function display_func() {
  draw_density();
  if(dvel)
    draw_velocity();
}
